package com.example.examena

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnEnviar = findViewById<Button>(R.id.btnEnviar)
        val nombre = findViewById<TextView>(R.id.txtNombre)
        val edad = findViewById<TextView>(R.id.txtEdad)
        btnEnviar.setOnClickListener{
            val intent=Intent(this,Saludo::class.java)

            intent.putExtra("nombre", nombre.text.toString())
            intent.putExtra("edad", edad.text.toString())
            startActivity(intent)
        }
    }
}
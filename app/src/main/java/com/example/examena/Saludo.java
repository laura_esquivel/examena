package com.example.examena;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Saludo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saludo);
        String nombre = getIntent().getExtras().getString("nombre");
        String edad = getIntent().getExtras().getString("edad");

        TextView lblSaludo = (TextView)findViewById(R.id.lblSaludo);
        lblSaludo.setText("Hola " + nombre + " tu edad es " + edad + " años.");
    }
}